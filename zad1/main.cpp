/*plik glowny dla zadania 1 z listy 1 */
//generowanie dynamicznej tablicy dwuwymiarowej
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "operacje.hh"
using namespace std;


int main()
{
  srand(time(NULL)); //odczyt czasu potrzebny do losowania liczb
  int m,n;
  int wyb,i,j,x;
  int **tab;
  int tmp=0;
  while(1) //menu
    {
      cout<<"1.Generuj tablice"<<endl;
      cout<<"2.Wypelnij tablice losowymi liczbami z wlasnego zakresu"<<endl;
      cout<<"3.Wyswietl tablice"<<endl;
      cout<<"4.Znajdz najwieksza liczbe na tablicy"<<endl;
      cout<<"0.Koniec"<<endl;
      cin>>wyb;
      switch(wyb)
	{
	case 1:
	  {
	    cout <<"Podaj liczbe wierszy: "<<endl;
	    cin >>m;
	    cout <<"Podaj liczbe kolumn: "<<endl;
	    cin >>n;
	    tab=(int**)malloc(m*n); //alokacja pami�ci
	    for(i=0;i<m;++i)
	      {
		tab[i]=new int [n];
		for(j=0;j<n;j++)
		  {
		    tab[i][j]=0; //wypelnienie tablicy zerami w celu unikniecia adresow w tablicy
		  }
	      }
	    break;
	  }
	case 2:
	  {
	    cout<<"Podaj maksymalna liczbe: "<<endl;
	    cin>>x;
	    for(i=0;i<m;++i)
	      {
		for(j=0;j<n;++j)
		  {
		    tab[i][j]=rand()%x; //wypelnianie tablicy losowymi liczbami z zakresu 0 do x, gdzie x wprowadza uzytkownik
		    
		  }
	      }
	    break;
	  }
	case 3:
	  {
	    for(i=0;i<m;i++)
		  {
		    for(j=0;j<n;j++)
		      {
			cout <<tab[i][j]<<" "; //wyswietlanie tablicy
		      }
		    cout <<endl;
		  }
		break;
	  }
	case 4:
	  {
	  	tmp=tab[0][0];
	    for(int ii=0;ii<m;ii++)
	      {
	      	for(int jj=0;jj<n;jj++)
	      	{
			  if(tab[ii][jj]>=tmp) //algorytm wyszukiwania najwiekszej liczby w tablicy
		  	{	
		    	tmp=tab[ii][jj];
		  	}
		  	}
	      }
	    cout<<"Najwieksza liczba to: "<<tmp<<endl;
	    break;
	  }
	case 0:
	  {
	    for(int k=0;k<n;++k)
	      
		delete [] tab[k];
	      
	    delete [] tab;
	   
	    return 0;
	    break;
	  }
	}
    }
}
