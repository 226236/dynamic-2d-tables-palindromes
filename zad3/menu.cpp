/*plik zawierajacy definicje funkcji menu */
#include <iostream>
#include "dzialania.hh"
#include "menu.hh"
using namespace std;

int menu ()
{
  int wyb,x,p;
  while(1)
    {
      cout <<"1. Potega"<<endl; /*opis menu programu */
      cout <<"2. Silnia"<<endl;
      cout <<"0. Koniec"<<endl;
      cin >> wyb;   /*moment podjecia decyzji o wyborze dzialania */
      switch(wyb) 
	{
	case 1:  /*potegowanie */
	  {
	    cout <<"Podaj podstawe: "<<endl;
	    cin >>x;
	    cout <<"Podaj wykladnik: "<<endl;
	    cin >>p;
	    Potega(x,p);
	    break;
	  }
	case 2:
	  {
	    cout <<"Podaj argument: "<<endl;
	    cin >>x;
	    Silnia(x);
	    break;
	  }
	case 0: /* koniec programu */
	  {
	    return 0;
	    break;
	  }
	default:
	  {
	    cout <<"Nie ma takiej opcji!"<<endl;
	  }
	}
    }
}
