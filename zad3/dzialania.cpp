/*Plik zaiwerajacy definicje funkcji potegi i silni */
/*DO ZROBIENIA/NAPRAWY:   */
#include <iostream>
#include "dzialania.hh"
using namespace std;

/*definicja funkcji potegowania opisanej rekurencyjnie */
int Potega(int x, int p)
{
  int i,temp;
  float y;
  y=1;
  if(p>=0)
    {
      for(i=1;i<=p;i++)
	{
	  y=y*x;
	}
    }
  else
    {
      p=-p;
      for(i=1;i<=p;i++)
	{
	  y=y*x;
	}
      y=1/y;
    }
  cout <<"Wynik potegowania: "<<y<<endl;
  return y;
}

/*definicja funkcji silni opisana rekurencyjnie */
int Silnia(int x)
{
  int i;
  double y=1;
  if(x<0)
    {
      cout << "Silnia tylko dla dodatnich liczb!"<<endl;
    }
  else
    {
      for(i=1;i<=x;i++)
	{
	  y=y*i;
	}
      cout <<"Wynik silni: "<<y<<endl;
    }
  return y;
}
  
